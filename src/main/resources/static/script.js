

  function showUserCreateBox() {
    Swal.fire({
      title: 'Create user',
      html:
        '<input id="id" type="hidden">' +
        '<input id="first_name" class="swal2-input" placeholder="First">' +
        '<input id="last_name" class="swal2-input" placeholder="Last">' +
        '<input id="username" class="swal2-input" placeholder="Username">' +
        '<input id="email" class="swal2-input" placeholder="Email">' +
        '<input id="password" class="swal2-input" placeholder="Password">' +
        '<input id="number" class="swal2-input" placeholder="Number">' +
        '<input id="street" class="swal2-input" placeholder="Street">' +
        '<input id="city" class="swal2-input" placeholder="City">' +
        '<input id="state" class="swal2-input" placeholder="State">',
      focusConfirm: false,
      preConfirm: () => {
        userCreate();
      }
    })
  }
  
  function userCreate() {
    const fname = document.getElementById("first_name").value;
    const lname = document.getElementById("last_name").value;
    const username = document.getElementById("username").value;
    const email = document.getElementById("email").value;
    const number = document.getElementById("number").value;
    const password = document.getElementById("password").value;
    const city = document.getElementById("city").value;
    const street = document.getElementById("street").value;
    const state = document.getElementById("state").value;

  
    const xhttp = new XMLHttpRequest();
    xhttp.open("POST", "http://localhost:8080/api/user"); 
    xhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  
    const address = {
      street: street, 
      city: city,
      state: state
    };
  
    const payload = {
      first_name: fname,
      last_name: lname,
      username:username,
      email: email,
      password: password, 
      phoneNumber: number,
      address: address
    };
  
    xhttp.send(JSON.stringify(payload));
  
    xhttp.onreadystatechange = function() {
      if (this.readyState === 4 && this.status === 200) {
        const response = JSON.parse(this.responseText);
        Swal.fire(response['message']);
        loadTable();
      }
    };
  }