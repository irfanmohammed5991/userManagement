package org.ckr.usermanagement.service;

import java.util.List;
import java.util.Optional;

import org.ckr.usermanagement.model.User;
import org.springframework.beans.factory.annotation.Qualifier;


public interface UserService {

    User create(User user);

    List<User> getAll();

    User updateUser(User user, long id);

    void changePasswordUser(User user, long id);

    void deleteUser(long id);

    Optional<User> getUser(long id);
    
}
