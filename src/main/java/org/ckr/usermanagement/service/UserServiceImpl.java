package org.ckr.usermanagement.service;

import java.util.List;
import java.util.Optional;

import org.ckr.usermanagement.model.User;
import org.ckr.usermanagement.rabbitMq.RabbitMqProducer;
import org.ckr.usermanagement.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    private RabbitMqProducer rabbitMqProducer;
    

    public UserServiceImpl(RabbitMqProducer rabbitMqProducer) {
        this.rabbitMqProducer = rabbitMqProducer;
    }

    @Override
    public User create(User user) {
        String password = user.getPassword();
        if(password.length()<1) {
            user.setPassword(generatePassword());
        }
        User dbUser = userRepository.save(user);
        
        return dbUser;
    }

    private String generatePassword() {
        final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()";
        final int PASSWORD_LENGTH = 10;

        StringBuilder password = new StringBuilder();
        for (int i = 0; i < PASSWORD_LENGTH; i++) {
            int index = (int) (Math.random() * CHARACTERS.length());
            password.append(CHARACTERS.charAt(index));
        }
        return password.toString();

    
    }

    @Override
    public List<User> getAll() {
        List<User> users = userRepository.findAll();
        return users;
    }

    @Override
    public User updateUser(User user, long id) {
        User updatedUser = userRepository.save(user);
        return updatedUser;
    }

    @Override
    public void changePasswordUser(User user, long id) {
       
    

    }

    @Override
    public void deleteUser(long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Optional<User> getUser(long id) {
        // User user = userRepository.getReferenceById(id);
        Optional<User> user = userRepository.findById(id);
        return user;
    }
    
}
