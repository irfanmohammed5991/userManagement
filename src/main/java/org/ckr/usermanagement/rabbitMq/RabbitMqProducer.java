package org.ckr.usermanagement.rabbitMq;

import org.ckr.usermanagement.model.User;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class RabbitMqProducer {
    
    private String topicExchange = "irfanExchange";
    private String routingKey = "seven";

    private String routingKeyJson = "eight";

    private RabbitTemplate rabbitTemplate;

    public RabbitMqProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(String message) {

        rabbitTemplate.convertAndSend(topicExchange, routingKey, message);
    }

    public void sendJsonMesage(User  user) {
        rabbitTemplate.convertAndSend(topicExchange, routingKeyJson, user); 
    }
}
