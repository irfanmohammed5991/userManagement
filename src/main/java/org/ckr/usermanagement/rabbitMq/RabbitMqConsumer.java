package org.ckr.usermanagement.rabbitMq;

import org.ckr.usermanagement.model.User;
import org.ckr.usermanagement.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RabbitMqConsumer {

    @Autowired
    private UserRepository userRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMqConsumer.class);
    
    @RabbitListener(queues = "irfan_json")
    public void consumeJsonMessage(User user) {

        String password = user.getPassword();
        if(password.length()<1) {
            user.setPassword(generatePassword());
        }
        User dbUser = userRepository.save(user);
        
           LOGGER.info("Received message:{} " + dbUser);
        
    }

    private String generatePassword() {
        final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()";
        final int PASSWORD_LENGTH = 10;

        StringBuilder password = new StringBuilder();
        for (int i = 0; i < PASSWORD_LENGTH; i++) {
            int index = (int) (Math.random() * CHARACTERS.length());
            password.append(CHARACTERS.charAt(index));
        }
        return password.toString();

    
    }
}
