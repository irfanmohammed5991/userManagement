package org.ckr.usermanagement.controller;

import java.util.List;
import java.util.Optional;

import org.ckr.usermanagement.model.User;
import org.ckr.usermanagement.rabbitMq.RabbitMqProducer;
import org.ckr.usermanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "/api/user")
public class Controller {

    @Autowired
    @Qualifier("userServiceImpl")
    private UserService userService;

    private RabbitMqProducer rabbitMqProducer;
    

    public Controller(RabbitMqProducer rabbitMqProducer) {
        this.rabbitMqProducer = rabbitMqProducer;
    }

    @PostMapping
    public ResponseEntity<?> createUser(@RequestBody User user) {
        try {
            // User createdUser = userService.create(user);
            rabbitMqProducer.sendJsonMesage(user);
            // return new ResponseEntity<User>(createdUser, HttpStatus.CREATED);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    } 


    @GetMapping("/publish")
    public ResponseEntity<String> sendMessage(@RequestParam("message") String message) {
        rabbitMqProducer.sendMessage(message);
        return ResponseEntity.ok("irfan is sending the message......");
    }

    @GetMapping
    public List<User> getUsers() {
         // return new ResponseEntity<User>(createdUser, HttpStatus.CREATED);
        List<User> users = userService.getAll();
        return users;
    }

    @GetMapping("/{id}")
    public Optional<User> getUser(@PathVariable long id) {
        Optional<User> user = userService.getUser(id);
        return user;
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> update(@RequestBody User user, @PathVariable long id) {
        try {
            User updatedUser = userService.updateUser(user, id);
            return new ResponseEntity<User>(updatedUser, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable long id) {
        userService.deleteUser(id);
    }

    @PatchMapping
    public void changePassword(@RequestBody User user, @PathVariable long id) {
        userService.changePasswordUser(user, id);
    }
}
