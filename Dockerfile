FROM openjdk
ADD target/usermanagement-0.0.1-SNAPSHOT.jar UM.jar
CMD ["java", "-jar", "UM.jar"]